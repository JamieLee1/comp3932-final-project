## Name

JAMES LEE - FINAL YEAR PROJECT

## Description

The aim of this project is to create an application capable of benchmarking 2 blockchain systems containing smart contracts, allowing for comparisons to be made between them in terms of performance, scalability and efficiency.

This project relies on:

The modification of Hyperledger Fabric's sample networks. Instructions on how to download and run these can be found [here](https://hyperledger-fabric.readthedocs.io/en/latest/getting_started.html). Modifications were made to the test-network such that only 1 peer need verify a transaction in order for it to be accepted as valid. The "assetTransfer" chaincode was also modified, however this has already been copied into this repo and can be found in "src\main\resources\assetTransferCustom".

Go Ethereum, the docs for which can be viewed [here](https://geth.ethereum.org/docs/getting-started). The files can be seen within this repo.
