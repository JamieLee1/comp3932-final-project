/*
 * BASED ON ASSET-TRANSFER-BASIC FROM FABRIC-SAMPLES
 *
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
/* CREATED BY JAMES LEE 18/01/2022 */

"use strict";

const stringify = require("json-stringify-deterministic");
const sortKeysRecursive = require("sort-keys-recursive");
const { Contract } = require("fabric-contract-api");
let numberOfClients = 0;

class ClientRecords extends Contract {
  /**
   * Initialises the ledger, resetting the array of clients
   *
   * @param ctx The transaction context
   */
  async initLedger(ctx) {
    const clients = [];

    for (const client of clients) {
      client.docType = "client";
      await ctx.stub.putState(
        client.clientId,
        Buffer.from(stringify(sortKeysRecursive(client)))
      );
    }
  }

  /**
   * createClient issues a new client to the world state with given details.
   *
   * @param {Context} ctx The transaction context
   * @param {String} _clientName Text containing the name of the client to store
   * @return {json} JSON string of newly created client record
   */
  async createClient(ctx, _clientName) {
    const client = {
      clientId: numberOfClients.toString(),
      clientName: _clientName.toString(),
      recordOwner: ctx.clientIdentity.getID().toString(),
    };

    const success = await ctx.stub.putState(
      numberOfClients.toString(),
      Buffer.from(stringify(sortKeysRecursive(client)))
    );
    if (success) {
      numberOfClients += 1;
    }
    return JSON.stringify(client);
  }

  /**
   * getClient returns the client stored in the world state with given id.
   *
   * @param {Context} ctx The transaction context
   * @param {String} _clientId ID of the client whose record we want to retrieve
   * @return {json} JSON string of requested client record
   */
  async getClient(ctx, _clientId) {
    const clientJSON = await ctx.stub.getState(_clientId);
    if (!clientJSON || clientJSON.length === 0) {
      throw new Error(`The client ${_clientId} does not exist`);
    }

    return clientJSON.toString();
  }

  /**
   * updateClientName updates an existing client in the world state with provided new name.
   *
   * @param {Context} ctx The transaction context
   * @param {String} _clientId ID of the client whose record we want to retrieve
   * @param {String} _newName Text containing the new name of the client to store on the record
   * @return {json} JSON string of updated client record
   */
  async updateClientName(ctx, _clientId, _newName) {
    const clientJSON = await ctx.stub.getState(_clientId);
    if (!clientJSON || clientJSON.length === 0) {
      throw new Error(`The client ${_clientId} does not exist`);
    }

    const requester = ctx.clientIdentity.getID().toString();

    if (JSON.parse(clientJSON).recordOwner !== requester) {
      throw new Error(`Incorrect Permissions`);
    }

    // overwriting original client with new client
    const updatedClient = {
      clientId: _clientId.toString(),
      clientName: _newName.toString(),
      recordOwner: ctx.clientIdentity.getID().toString(),
    };

    ctx.stub.putState(
      _clientId,
      Buffer.from(stringify(sortKeysRecursive(updatedClient)))
    );
    return JSON.stringify(updatedClient);
  }
}

module.exports = ClientRecords;
