/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
/* MODIFIED BY JAMES LEE 18/01/2022 */

'use strict';

const clientRecords = require('./lib/clientRecords.js');

module.exports.ClientRecords = clientRecords;
module.exports.contracts = [clientRecords];
