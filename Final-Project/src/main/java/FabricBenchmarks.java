import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.commons.io.FileUtils;
import org.hyperledger.fabric.gateway.Contract;
import org.hyperledger.fabric.gateway.Gateway;
import org.hyperledger.fabric.gateway.Identities;
import org.hyperledger.fabric.gateway.Identity;
import org.hyperledger.fabric.gateway.Network;
import org.hyperledger.fabric.gateway.Wallet;
import org.hyperledger.fabric.gateway.Wallets;
import org.hyperledger.fabric.gateway.X509Identity;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric.sdk.security.CryptoSuiteFactory;
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;

// ORIGINAL CODE FOR ENROLLADMIN TAKEN FROM FABRIC-SAMPLES/ASSET-TRANSFER-BASIC/APPLICATION-JAVA/ENROLL.JAVA
/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
class EnrollAdmin {

  public static void run() throws Exception {
    // Create a CA client for interacting with the CA.
    Properties props = new Properties();
    props.put(
      "pemFile",
      "../FABRIC/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem"
    );
    props.put("allowAllHostNames", "true");
    HFCAClient caClient = HFCAClient.createNewInstance(
      "https://localhost:7054",
      props
    );
    CryptoSuite cryptoSuite = CryptoSuiteFactory.getDefault().getCryptoSuite();
    caClient.setCryptoSuite(cryptoSuite);

    // Create a wallet for managing identities
    Wallet wallet = Wallets.newFileSystemWallet(Paths.get("wallet"));

    // Check to see if we've already enrolled the admin user.
    if (wallet.get("admin") != null) {
      System.out.println(
        "An identity for the admin user \"admin\" already exists in the wallet"
      );
      return;
    }

    // Enroll the admin user, and import the new identity into the wallet.
    final EnrollmentRequest enrollmentRequestTLS = new EnrollmentRequest();
    enrollmentRequestTLS.addHost("localhost");
    enrollmentRequestTLS.setProfile("tls");
    Enrollment enrollment = caClient.enroll(
      "admin",
      "adminpw",
      enrollmentRequestTLS
    );
    Identity user = Identities.newX509Identity("Org1MSP", enrollment);
    wallet.put("admin", user);
    System.out.println(
      "Successfully enrolled user \"admin\" and imported it into the wallet"
    );
  }
}

// ORIGINAL CODE FOR REGISTERUSER TAKEN FROM FABRIC-SAMPLES/ASSET-TRANSFER-BASIC/APPLICATION-JAVA/REGISTERUSER.JAVA
/*
SPDX-License-Identifier: Apache-2.0
*/
class RegisterUser {

  public static void run(int userNumber) throws Exception {
    // Create a CA client for interacting with the CA.
    Properties props = new Properties();
    props.put(
      "pemFile",
      "../FABRIC/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem"
    );
    props.put("allowAllHostNames", "true");
    HFCAClient caClient = HFCAClient.createNewInstance(
      "https://localhost:7054",
      props
    );
    CryptoSuite cryptoSuite = CryptoSuiteFactory.getDefault().getCryptoSuite();
    caClient.setCryptoSuite(cryptoSuite);

    // Create a wallet for managing identities
    Wallet wallet = Wallets.newFileSystemWallet(Paths.get("wallet"));

    // Check to see if we've already enrolled the user.
    if (wallet.get("appUser" + String.valueOf(userNumber)) != null) {
      System.out.println(
        "An identity for the user \"appUser" +
        String.valueOf(userNumber) +
        "\" already exists in the wallet"
      );
      return;
    }

    X509Identity adminIdentity = (X509Identity) wallet.get("admin");
    if (adminIdentity == null) {
      System.out.println(
        "\"admin\" needs to be enrolled and added to the wallet first"
      );
      return;
    }
    User admin = new User() {
      @Override
      public String getName() {
        return "admin";
      }

      @Override
      public Set<String> getRoles() {
        return null;
      }

      @Override
      public String getAccount() {
        return null;
      }

      @Override
      public String getAffiliation() {
        return "org1.department1";
      }

      @Override
      public Enrollment getEnrollment() {
        return new Enrollment() {
          @Override
          public PrivateKey getKey() {
            return adminIdentity.getPrivateKey();
          }

          @Override
          public String getCert() {
            return Identities.toPemString(adminIdentity.getCertificate());
          }
        };
      }

      @Override
      public String getMspId() {
        return "Org1MSP";
      }
    };

    // Register the user, enroll the user, and import the new identity into the wallet.
    RegistrationRequest registrationRequest = new RegistrationRequest(
      "appUser" + String.valueOf(userNumber)
    );
    registrationRequest.setAffiliation("org1.department1");
    registrationRequest.setEnrollmentID("appUser" + String.valueOf(userNumber));
    String enrollmentSecret = caClient.register(registrationRequest, admin);
    Enrollment enrollment = caClient.enroll(
      "appUser" + String.valueOf(userNumber),
      enrollmentSecret
    );
    Identity user = Identities.newX509Identity("Org1MSP", enrollment);
    wallet.put("appUser" + String.valueOf(userNumber), user);
    System.out.println(
      "Successfully enrolled user \"appUser" +
      String.valueOf(userNumber) +
      "\" and imported it into the wallet"
    );
  }
}

public class FabricBenchmarks {

  /**
   * The initiateFabric class executes fabric commands
   * and starts the network
   */
  static void initiateFabric() throws IOException {
    System.out.println("Starting network...");

    int chaincodesInstalled = 0;

    //Executing command to initiate fabric
    String command =
      "wsl.exe cd ../FABRIC/fabric-samples/test-network/; ./network.sh up createChannel -c mychannel -ca; ./network.sh deployCC -ccn assetTransferCustom -ccp ../../../Final-Project/src/main/resources/assetTransferCustom/ -ccl javascript; ./network.sh deployCC -ccn clientRecords -ccp ../../../Final-Project/src/main/resources/fabricClientRecords/ -ccl javascript";
    Process wslRunCommand = Runtime.getRuntime().exec(command);

    // Close process and output any errors
    wslRunCommand.getOutputStream().close();

    BufferedReader input = new BufferedReader(
      new InputStreamReader(wslRunCommand.getInputStream())
    );

    while ((input.readLine()) != null) {
      if (
        input.readLine().contains("Chaincode initialization is not required")
      ) {
        chaincodesInstalled += 1;
        if (chaincodesInstalled == 2) {
          wslRunCommand.destroy();
        }
      }
    }
    input.close();
    System.out.println("Network started.");
  }

  /**
   * Transfer an asset from its current owner to owner99
   * over the Fabric network
   *
   * @param assetID  Integer value representing the ID of the asset to transfer
   *                 (also represents the user ID of the same value)
   * @return         Time taken to send and confirm the transaction (long value)
   */
  static long singleTransaction(int assetID) throws Exception {
    long totalTime = 0;

    try (Gateway gateway = connect(assetID)) {
      Network network = gateway.getNetwork("mychannel");
      Contract contract = network.getContract("assetTransferCustom");
      long timeOfSending = System.currentTimeMillis();
      contract.submitTransaction(
        "TransferAsset",
        "asset" + String.valueOf(assetID),
        "owner99"
      );

      long timeOfConfirmation = System.currentTimeMillis();

      totalTime = timeOfConfirmation - timeOfSending;
    } catch (Exception e) {
      System.err.println(e);
    }

    return totalTime;
  }

  /**
   * Send a single createClient request over the Fabric network to
   * the deployed chaincode (smart contract)
   *
   * @param userID   Integer value representing the ID of the user to connect to the
   *                 Fabric gateway from
   * @return         Time taken to send and confirm the request (long value)
   */
  static long createClientRequest(int userID) {
    long totalTime = 0;

    try (Gateway gateway = connect(userID)) {
      Network network = gateway.getNetwork("mychannel");
      Contract contract = network.getContract("clientRecords");
      long timeOfSending = System.currentTimeMillis();
      contract.submitTransaction(
        "createClient",
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
      );

      long timeOfConfirmation = System.currentTimeMillis();

      totalTime = timeOfConfirmation - timeOfSending;
    } catch (Exception e) {
      System.err.println(e);
    }

    return totalTime;
  }

  /**
   * Send a single getClient request over the Fabric network to
   * the deployed chaincode (smart contract)
   *
   * @param userID   Integer value representing the ID of the user to connect to the
   *                 Fabric gateway from
   * @return         Time taken to send request and recieve the returned value (long value)
   */
  static long getClientRequest(int userID) {
    long totalTime = 0;

    try (Gateway gateway = connect(userID)) {
      Network network = gateway.getNetwork("mychannel");
      Contract contract = network.getContract("clientRecords");
      long timeOfSending = System.currentTimeMillis();
      contract.submitTransaction("getClient", "0");

      long timeOfConfirmation = System.currentTimeMillis();

      totalTime = timeOfConfirmation - timeOfSending;
    } catch (Exception e) {
      System.err.println(e);
    }

    return totalTime;
  }

  /**
   * Executes the singleTransaction method multiple times,
   * saving the transaction times to a file
   *
   * @param file   File object where the results of each transaction should be written to
   */
  static void singleTransactionBenchmark(File file) throws Exception {
    try (PrintWriter writer = new PrintWriter(file)) {
      StringBuilder titles = new StringBuilder();
      titles.append("Transaction No.");
      titles.append(',');
      titles.append("Time (Milliseconds)");

      writer.println(titles.toString());

      for (int i = 0; i < 100; i++) {
        StringBuilder content = new StringBuilder();
        content.append(String.valueOf(i + 1));
        content.append(',');
        content.append(String.valueOf(singleTransaction(i)));
        content.append('\n');

        writer.write(content.toString());

        System.out.println("Written transaction " + (i + 1));
      }

      writer.flush();
      writer.close();
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Executes the createClientRequest or getClientRequest methods,
   * depending on supplied arguments, a number of times. Saves the recorded
   * time to a file
   *
   * @param typeOfRequest   String containing either "createClient" or "getClient", indicating
   *                        which type of smart contract request to initiate
   * @param file            File object where the results of each transaction should be written to
   */
  static void smartContractTransactionBenchmark(
    String typeOfRequest,
    File file
  ) throws Exception {
    try (PrintWriter writer = new PrintWriter(file)) {
      StringBuilder titles = new StringBuilder();
      titles.append("Transaction No.");
      titles.append(',');
      titles.append("Time (Milliseconds)");

      writer.println(titles.toString());

      for (int i = 0; i < 100; i++) {
        StringBuilder content = new StringBuilder();
        content.append(String.valueOf(i + 1));
        content.append(',');
        if (typeOfRequest.equals("createClient")) {
          content.append(createClientRequest(i));
        } else if (typeOfRequest.equals("getClient")) {
          content.append(getClientRequest(i));
        } else {
          throw new Exception("Incorrect request type");
        }
        content.append('\n');

        writer.write(content.toString());

        System.out.println("Written transaction " + (i + 1));
      }

      writer.flush();
      writer.close();
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * The SendMultipleTransactions class enables concurrent execution
   * of the singleTransaction method, by implementing Runnable (so can be
   * used with threads). The returned values are stored in a BlockingQueue
   * to ensure results are stored properly, and no results are lost by attempting
   * to write directly to a file.
   *
   * @param assetID   Integer value representing the ID of the asset to transfer
   *                  (also represents the user ID of the same value)
   * @param queue     Queue object used to store the results of each thread's transaction
   */
  static class SendMultipleTransactions implements Runnable {

    private int assetID;
    private BlockingQueue<Long> queue;

    SendMultipleTransactions(int assetID, BlockingQueue<Long> queue) {
      this.assetID = assetID;
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        queue.put(singleTransaction(assetID));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * The SendMultipleSmartContractTransactions class enables concurrent execution
   * of either the createClientRequest and getClientRequest methods, depending on
   * the supplied arugments, by implementing Runnable (so can be used with threads).
   * The returned values are stored in a BlockingQueue to ensure results are stored
   * properly, and no results are lost by attempting to write directly to a file.
   *
   * @param userID          Integer value representing the ID of the user to connect to the
   *                        Fabric gateway from
   * @param typeOfRequest   String containing either "createClient" or "getClient", indicating
   *                        which type of smart contract request to initiate
   * @param queue           Queue object used to store the results of each thread's smart contract
   *                        request
   */
  static class SendMultipleSmartContractTransactions implements Runnable {

    private int userID;
    private String typeOfRequest;
    private BlockingQueue<Long> queue;

    SendMultipleSmartContractTransactions(
      int userID,
      String typeOfRequest,
      BlockingQueue<Long> queue
    ) {
      this.userID = userID;
      this.typeOfRequest = typeOfRequest;
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        if (typeOfRequest.equals("createClient")) {
          queue.put(createClientRequest(userID));
        } else if (typeOfRequest.equals("getClient")) {
          queue.put(getClientRequest(userID));
        } else {
          throw new Exception("Incorrect request type");
        }
      } catch (Exception e) {
        // Print any other exceptions
        e.printStackTrace();
      }
    }
  }

  /**
   * The WriteMultipleTransactions class takes the BlockingQueues from
   * SendMultipleTransactions and SendMultipleSmartContractTransactions and
   * writes the stored values to a specified file.
   *
   * @param queue   Queue object that contains the results to be written to a file
   * @param file    File object indicating which file the results should be written to
   */
  static class WriteMultipleTransactions implements Runnable {

    private BlockingQueue<Long> queue;
    private File file;

    WriteMultipleTransactions(BlockingQueue<Long> queue, File file) {
      this.queue = queue;
      this.file = file;
    }

    @Override
    public void run() {
      try (PrintWriter writer = new PrintWriter(file)) {
        int i = 1;
        StringBuilder titles = new StringBuilder();
        titles.append("Transaction No.");
        titles.append(',');
        titles.append("Time (Milliseconds)");
        writer.println(titles.toString());

        while (!queue.isEmpty()) {
          try {
            StringBuilder content = new StringBuilder();
            content.append(String.valueOf(i));
            content.append(',');
            content.append(queue.take());
            content.append('\n');

            writer.write(content.toString());

            System.out.println("Written transaction " + (i));
            i += 1;
          } catch (InterruptedException e) {
            // Print any interrupt exceptions
            e.printStackTrace();
          }
        }

        writer.flush();
        writer.close();
      } catch (FileNotFoundException e) {
        // Print any exceptions wtih finding the files
        e.printStackTrace();
      }
    }
  }

  /**
   * The multipleTransactionBenchmark method creates a number of threads
   * (based on value of numberOfTransactions), using the SendMultipleTransactions
   * class to benchmark multiple concurrent transactions. Another thread is then
   * created to collect the results using the WriteMultipleTransactions class.
   *
   * @param file                  File object where the results of each transaction should be written to
   * @param numberOfTransactions  Integer value indicating the number of concurrent transactions to be made, each transaction
   *                              requires a new thread
   */
  static void multipleTransactionBenchmark(File file, int numberOfTransactions)
    throws InterruptedException, IOException {
    Thread t[] = new Thread[numberOfTransactions];

    BlockingQueue<Long> queue = new LinkedBlockingQueue<Long>(
      numberOfTransactions
    );

    for (int i = 0; i < numberOfTransactions; i++) {
      t[i] = new Thread(new SendMultipleTransactions(i, queue));
      t[i].start();
    }

    for (int i = 0; i < numberOfTransactions; i++) {
      t[i].join();
    }

    Thread writeToFile = new Thread(new WriteMultipleTransactions(queue, file));

    writeToFile.start();
    writeToFile.join();
  }

  /**
   * The multipleSmartContractTransactionBenchmark method creates a number of threads
   * (based on value of numberOfTransactions), using the SendMultipleSmartContractTransactions
   * class to benchmark multiple concurrent smart contract requests (with the request sent depending on
   * the value of typeOfRequest). Another thread is then created to collect the results using the
   * WriteMultipleTransactions class.
   *
   * @param typeOfRequest         String containing either "createClient" or "getClient", indicating
   *                              which type of smart contract request to initiate
   * @param file                  File object where the results of each transaction should be written to
   * @param numberOfTransactions  Integer value indicating the number of concurrent transactions to be made, each transaction
   *                              requires a new thread
   */
  static void multipleSmartContractTransactionsBenchmark(
    String typeOfRequest,
    File file,
    int numberOfTransactions
  ) throws InterruptedException, IOException {
    {
      Thread t[] = new Thread[numberOfTransactions];

      BlockingQueue<Long> queue = new LinkedBlockingQueue<Long>(
        numberOfTransactions
      );

      for (int i = 0; i < numberOfTransactions; i++) {
        t[i] =
          new Thread(
            new SendMultipleSmartContractTransactions(i, typeOfRequest, queue)
          );
        t[i].start();
      }

      for (int i = 0; i < numberOfTransactions; i++) {
        t[i].join();
      }

      Thread writeToFile = new Thread(
        new WriteMultipleTransactions(queue, file)
      );

      writeToFile.start();
      writeToFile.join();
    }
  }

  // TAKEN FROM FABRIC-SAMPLES/ASSET-TRANSFER-BASIC/APPLICATION-JAVA/APP.JAVA
  static {
    System.setProperty(
      "org.hyperledger.fabric.sdk.service_discovery.as_localhost",
      "true"
    );
  }

  // METHOD TAKEN FROM FABRIC-SAMPLES/ASSET-TRANSFER-BASIC/APPLICATION-JAVA/APP.JAVA
  // PATH "walletPath" MODIFIED BY JAMES LEE
  // helper function for getting connected to the gateway
  public static Gateway connect(int userNumber) throws Exception {
    // Load a file system based wallet for managing identities.
    Path walletPath = Paths.get("./wallet");
    Wallet wallet = Wallets.newFileSystemWallet(walletPath);
    // load a CCP
    Path networkConfigPath = Paths.get(
      "..",
      "FABRIC",
      "fabric-samples",
      "test-network",
      "organizations",
      "peerOrganizations",
      "org1.example.com",
      "connection-org1.yaml"
    );

    Gateway.Builder builder = Gateway.createBuilder();
    builder
      .identity(wallet, "appUser" + String.valueOf(userNumber))
      .networkConfig(networkConfigPath)
      .discovery(true);
    return builder.connect();
  }

  /**
   * The stopFabric() method terminates the Fabric process,
   * wiping the blockchain of all data.
   */
  static void stopFabric() throws IOException {
    System.out.println("Stopping network...");

    //Executing command to stop fabric
    String command =
      "wsl.exe cd ../FABRIC/fabric-samples/test-network/; ./network.sh down";
    Process powerShellRunCommand = Runtime.getRuntime().exec(command);

    // Output any errors
    powerShellRunCommand.getOutputStream().close();
    BufferedReader input = new BufferedReader(
      new InputStreamReader(powerShellRunCommand.getInputStream())
    );
    while ((input.readLine()) != null) {}
    input.close();
    powerShellRunCommand.destroyForcibly();
    FileUtils.deleteDirectory(new File("./wallet"));
    System.out.println("Fabric Process Terminated");
  }

  /**
   * The initialiseLedger() method calls the InitLedger function
   * from the asset transfer chaincode (smart contract), resetting
   * the ledger ready for the benchmark tests
   */
  static void initialiseLedger() {
    try (Gateway gateway = connect(0)) {
      Network network = gateway.getNetwork("mychannel");
      Contract contract = network.getContract("assetTransferCustom");

      System.out.println("Initialising the ledger...");
      contract.submitTransaction("InitLedger");
    } catch (Exception e) {
      System.err.println(e);
    }
  }

  public static void main(String[] args) throws Exception {
    // Setting up files to be used by benchmark tests
    File[] transactionTestsFiles = new File[15];
    String[] transactionTestsFileNames = new String[] {
      "single",
      "50_concurrent",
      "100_concurrent",
      "150_concurrent",
      "200_concurrent",
    };

    for (int i = 0; i < transactionTestsFileNames.length; i++) {
      transactionTestsFiles[i] =
        new File(
          "./csvOutputs/fabric/basicTransactions/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i].exists()) {
        transactionTestsFiles[i].createNewFile();
      }
      transactionTestsFiles[i + 5] =
        new File(
          "./csvOutputs/fabric/smartContractCreateClient/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i + 5].exists()) {
        transactionTestsFiles[i + 5].createNewFile();
      }
      transactionTestsFiles[i + 10] =
        new File(
          "./csvOutputs/fabric/smartContractGetClient/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i + 10].exists()) {
        transactionTestsFiles[i + 10].createNewFile();
      }
    }

    // Start the Fabric network
    initiateFabric();

    //Creating admin account and users for benchmarks
    try {
      EnrollAdmin.run();
      for (int i = 0; i < 200; i++) {
        RegisterUser.run(i);
      }
    } catch (Exception e) {
      System.err.println(e);
    }

    // Single and concurrent tests for basic transactions

    initialiseLedger();

    singleTransactionBenchmark(transactionTestsFiles[0]);

    for (int i = 50; i < 201; i += 50) {
      initialiseLedger();
      multipleTransactionBenchmark(transactionTestsFiles[i / 50], i);
    }

    // Single and concurrent tests for createClient and getClient smart contract functions

    smartContractTransactionBenchmark("createClient", transactionTestsFiles[5]);

    smartContractTransactionBenchmark("getClient", transactionTestsFiles[10]);

    for (int i = 300; i < 451; i += 50) {
      multipleSmartContractTransactionsBenchmark(
        "createClient",
        transactionTestsFiles[i / 50],
        i - 250
      );
    }

    for (int i = 550; i < 701; i += 50) {
      multipleSmartContractTransactionsBenchmark(
        "getClient",
        transactionTestsFiles[i / 50],
        i - 500
      );
    }

    stopFabric();
  }
}
