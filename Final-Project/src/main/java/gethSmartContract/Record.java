package gethSmartContract;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.DynamicStruct;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.4.1.
 */
@SuppressWarnings("rawtypes")
public class Record extends Contract {
    public static final String BINARY = "0x608060405234801561001057600080fd5b50610a87806100206000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c806386b2fd4714610046578063d0068f8014610062578063e88e9f0f14610092575b600080fd5b610060600480360381019061005b9190610718565b6100ae565b005b61007c60048036038101906100779190610774565b610207565b60405161008991906108c9565b60405180910390f35b6100ac60048036038101906100a791906108eb565b61039e565b005b816001600082815260200190815260200160002060009054906101000a900460ff1661011157336040517f9b639a290000000000000000000000000000000000000000000000000000000081526004016101089190610943565b60405180910390fd5b82600081815481106101265761012561095e565b5b906000526020600020906003020160020160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16146101c757336040517fa1e8c63a0000000000000000000000000000000000000000000000000000000081526004016101be9190610943565b60405180910390fd5b82600085815481106101dc576101db61095e565b5b906000526020600020906003020160010190805190602001906102009291906104ae565b5050505050565b61020f610534565b816001600082815260200190815260200160002060009054906101000a900460ff1661027257336040517f9b639a290000000000000000000000000000000000000000000000000000000081526004016102699190610943565b60405180910390fd5b60008084815481106102875761028661095e565b5b9060005260206000209060030201604051806060016040529081600082015481526020016001820180546102ba906109bc565b80601f01602080910402602001604051908101604052809291908181526020018280546102e6906109bc565b80156103335780601f1061030857610100808354040283529160200191610333565b820191906000526020600020905b81548152906001019060200180831161031657829003601f168201915b505050505081526020016002820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152505090508092505050919050565b6000604051806060016040528060008054905081526020018381526020013373ffffffffffffffffffffffffffffffffffffffff1681525090806001815401808255809150506001900390600052602060002090600302016000909190919091506000820151816000015560208201518160010190805190602001906104259291906104ae565b5060408201518160020160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055505050600180600060016000805490506104869190610a1d565b815260200190815260200160002060006101000a81548160ff02191690831515021790555050565b8280546104ba906109bc565b90600052602060002090601f0160209004810192826104dc5760008555610523565b82601f106104f557805160ff1916838001178555610523565b82800160010185558215610523579182015b82811115610522578251825591602001919060010190610507565b5b509050610530919061056b565b5090565b60405180606001604052806000815260200160608152602001600073ffffffffffffffffffffffffffffffffffffffff1681525090565b5b8082111561058457600081600090555060010161056c565b5090565b6000604051905090565b600080fd5b600080fd5b6000819050919050565b6105af8161059c565b81146105ba57600080fd5b50565b6000813590506105cc816105a6565b92915050565b600080fd5b600080fd5b6000601f19601f8301169050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b610625826105dc565b810181811067ffffffffffffffff82111715610644576106436105ed565b5b80604052505050565b6000610657610588565b9050610663828261061c565b919050565b600067ffffffffffffffff821115610683576106826105ed565b5b61068c826105dc565b9050602081019050919050565b82818337600083830152505050565b60006106bb6106b684610668565b61064d565b9050828152602081018484840111156106d7576106d66105d7565b5b6106e2848285610699565b509392505050565b600082601f8301126106ff576106fe6105d2565b5b813561070f8482602086016106a8565b91505092915050565b6000806040838503121561072f5761072e610592565b5b600061073d858286016105bd565b925050602083013567ffffffffffffffff81111561075e5761075d610597565b5b61076a858286016106ea565b9150509250929050565b60006020828403121561078a57610789610592565b5b6000610798848285016105bd565b91505092915050565b6107aa8161059c565b82525050565b600081519050919050565b600082825260208201905092915050565b60005b838110156107ea5780820151818401526020810190506107cf565b838111156107f9576000848401525b50505050565b600061080a826107b0565b61081481856107bb565b93506108248185602086016107cc565b61082d816105dc565b840191505092915050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b600061086382610838565b9050919050565b61087381610858565b82525050565b600060608301600083015161089160008601826107a1565b50602083015184820360208601526108a982826107ff565b91505060408301516108be604086018261086a565b508091505092915050565b600060208201905081810360008301526108e38184610879565b905092915050565b60006020828403121561090157610900610592565b5b600082013567ffffffffffffffff81111561091f5761091e610597565b5b61092b848285016106ea565b91505092915050565b61093d81610858565b82525050565b60006020820190506109586000830184610934565b92915050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052603260045260246000fd5b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b600060028204905060018216806109d457607f821691505b602082108114156109e8576109e761098d565b5b50919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b6000610a288261059c565b9150610a338361059c565b925082821015610a4657610a456109ee565b5b82820390509291505056fea2646970667358221220184e7b791a13dfc47acaa375622b89d681827a5a45888f7772ad419e23353ebf64736f6c634300080b0033";

    public static final String FUNC_CREATECLIENT = "createClient";

    public static final String FUNC_UPDATECLIENTNAME = "updateClientName";

    public static final String FUNC_GETCLIENT = "getClient";

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<String, String>();
        _addresses.put("9089", "0x8Ee476B9E83EeFF790F4fac14b621a3ED8389576");
    }

    @Deprecated
    protected Record(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Record(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Record(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Record(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteFunctionCall<TransactionReceipt> createClient(String _clientName) {
        final Function function = new Function(
                FUNC_CREATECLIENT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_clientName)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> updateClientName(BigInteger _clientId, String _newName) {
        final Function function = new Function(
                FUNC_UPDATECLIENTNAME, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_clientId), 
                new org.web3j.abi.datatypes.Utf8String(_newName)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<Client> getClient(BigInteger _clientId) {
        final Function function = new Function(FUNC_GETCLIENT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_clientId)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Client>() {}));
        return executeRemoteCallSingleValueReturn(function, Client.class);
    }

    @Deprecated
    public static Record load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Record(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Record load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Record(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Record load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Record(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Record load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Record(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<Record> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Record.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Record> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Record.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Record> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Record.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Record> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Record.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class Client extends DynamicStruct {
        public BigInteger clientId;

        public String clientName;

        public String recordOwner;

        public Client(BigInteger clientId, String clientName, String recordOwner) {
            super(new org.web3j.abi.datatypes.generated.Uint256(clientId),new org.web3j.abi.datatypes.Utf8String(clientName),new org.web3j.abi.datatypes.Address(recordOwner));
            this.clientId = clientId;
            this.clientName = clientName;
            this.recordOwner = recordOwner;
        }

        public Client(Uint256 clientId, Utf8String clientName, Address recordOwner) {
            super(clientId,clientName,recordOwner);
            this.clientId = clientId.getValue();
            this.clientName = clientName.getValue();
            this.recordOwner = recordOwner.getValue();
        }
    }
}
