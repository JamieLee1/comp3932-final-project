import gethSmartContract.Record;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.*;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.utils.Convert;

public class GethBenchmarks {

  /**
   * The InitiateGeth class executes Geth commands
   * and starts the network
   */
  static class InitiateGeth implements Runnable {

    private static volatile boolean finished = false;

    @Override
    public void run() {
      try {
        initiateGeth();
      } catch (IOException e) {
        throw new RuntimeException("It all went horribly wrong!", e);
      }
    }

    public static void setFinished(boolean isFinished) {
      finished = isFinished;
    }

    static void initiateGeth() throws IOException {
      //Executing command to initiate geth
      String command =
        "powershell.exe  cd C:\\Users\\Jamie\\Desktop\\DIssertration\\GETH\\GETHMAIN; geth --datadir data --networkid 5605 --rpc.allow-unprotected-txs --http ";
      Process powerShellRunCommand = Runtime.getRuntime().exec(command);

      while (true) {
        if (finished == true) {
          powerShellRunCommand.destroyForcibly();
          break;
        }
      }

      System.out.println("Geth process terminated");
    }
  }

  /**
   * The InitiateNode class executes Geth commands
   * and starts a miner node
   */
  static class InitiateNode implements Runnable {

    private static volatile boolean finished = false;

    @Override
    public void run() {
      try {
        initiateNode();
      } catch (IOException e) {
        throw new RuntimeException("It all went horribly wrong!", e);
      }
    }

    public static void setFinished(boolean isFinished) {
      finished = isFinished;
    }

    static void initiateNode() throws IOException {
      //Executing command to initiate miner node
      String command =
        "powershell.exe  cd C:\\Users\\Jamie\\Desktop\\DIssertration\\GETH\\GETHNODE; geth --datadir node --networkid 5605 --port 30315 --bootnodes enr:-KO4QMyeojRdSTEUVJRecAZPtlgtz0Wr2a1ProHgmZAf83PQQAlaGvbcSGVWQoZkEDv9HrTDLxGIvnSsSWVpABw2pVeGAX6TSTJ1g2V0aMfGhDa9GvaAgmlkgnY0gmlwhFFtJOeJc2VjcDI1NmsxoQInuOZ0XEG4Ju9yT2Dj5igjmzGGQKAhqQ3BzZ4u8M1GqYRzbmFwwIN0Y3CCdl-DdWRwgnZf --ipcdisable --mine --miner.threads=1 --miner.etherbase=0xa51C7da7E06303c0CF1913dD0e0A896Ea72c5eC1";
      Process powerShellRunCommand = Runtime.getRuntime().exec(command);

      String addMiner =
        "powershell.exe  geth attach ipc:\\\\.\\pipe\\geth.ipc; admin.addPeer(\"enode://0e0ea51a2b0a36764dca33ea5e7282235d0a1a6d3955d72ec78e40f3d5a861613e4716097720d2d11a87df179c2a9e8e1157c6a3e26f6a110922db7561a8849c@127.0.0.1:30315\")";
      Process addMinerCommand = Runtime.getRuntime().exec(addMiner);

      addMinerCommand.destroy();

      while (true) {
        if (finished == true) {
          powerShellRunCommand.destroy();
          break;
        }
      }

      System.out.println("Node process terminated");
    }
  }

  /**
   * Send a single transaction of ETH over the Geth network
   *
   * @param web3j              The Web3j client used to interact with the Geth network
   * @param recievingAddress   String containing the ETH address of the recipient of the transaction to be made
   * @param value              BigDecimal value of the transaction, in WEI
   * @param credentials        The account credentials for the sender of the transaction, to allow the transaciton
   *                           to be made
   * @return                   Time taken to send and confirm the transaction (long value)
   */
  static long singleTransaction(
    Web3j web3j,
    String recievingAddress,
    BigDecimal value,
    Credentials credentials
  ) throws Exception {
    LocalDateTime timeOfSending = Instant
      .now()
      .atZone(ZoneId.of("UTC"))
      .toLocalDateTime();
    LocalDateTime timeOfConfirmation = Instant
      .now()
      .atZone(ZoneId.of("UTC"))
      .toLocalDateTime();

    TransactionReceipt transactionReceipt = Transfer
      .sendFunds(
        web3j,
        credentials,
        recievingAddress,
        value,
        Convert.Unit.ETHER
      )
      .send();

    BigInteger nextBlock = new BigInteger("1");

    while (true) {
      try {
        EthBlock ethBlock = web3j
          .ethGetBlockByNumber(
            DefaultBlockParameter.valueOf(
              (transactionReceipt.getBlockNumber().add(nextBlock))
            ),
            true
          )
          .send();

        timeOfConfirmation =
          Instant
            .ofEpochSecond(ethBlock.getBlock().getTimestamp().longValue())
            .atZone(ZoneId.of("UTC"))
            .toLocalDateTime();
        break;
      } catch (NullPointerException e) {
        System.out.println("Waiting for block.");
      }
    }

    long totalTime = Duration
      .between(timeOfSending, timeOfConfirmation)
      .toMillis();

    return totalTime;
  }

  /**
   * Send a single createClient request over the Geth network to
   * the deployed smart contract
   *
   * @param web3j              The Web3j client used to interact with the Geth network
   * @param deployedContract   The Record object representing the deployed smart contract
   * @return                   Time taken to send and confirm the request (long value)
   */
  static long createClientRequest(Record deployedContract, Web3j web3j)
    throws Exception {
    LocalDateTime timeOfSending = Instant
      .now()
      .atZone(ZoneId.of("UTC"))
      .toLocalDateTime();
    LocalDateTime timeOfConfirmation = Instant
      .now()
      .atZone(ZoneId.of("UTC"))
      .toLocalDateTime();

    TransactionReceipt transactionReceipt = deployedContract
      .createClient(
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
      )
      .send();

    BigInteger nextBlock = new BigInteger("1");

    while (true) {
      try {
        EthBlock ethBlock = web3j
          .ethGetBlockByNumber(
            DefaultBlockParameter.valueOf(
              (transactionReceipt.getBlockNumber().add(nextBlock))
            ),
            true
          )
          .send();

        timeOfConfirmation =
          Instant
            .ofEpochSecond(ethBlock.getBlock().getTimestamp().longValue())
            .atZone(ZoneId.of("UTC"))
            .toLocalDateTime();
        break;
      } catch (NullPointerException e) {
        System.out.println("Waiting for block.");
      }
    }

    long totalTime = Duration
      .between(timeOfSending, timeOfConfirmation)
      .toMillis();

    return totalTime;
  }

  /**
   * Send a single getClient request over the Geth network to
   * the deployed smart contract
   *
   * @param deployedContract   The Record object representing the deployed smart contract
   * @return                   Time taken to send request and recieve the returned value (long value)
   */
  static long getClientRequest(Record deployedContract) throws Exception {
    LocalDateTime timeOfSending = LocalDateTime.now();

    deployedContract.getClient(BigInteger.valueOf(0)).send();

    LocalDateTime timeOfConfirmation = LocalDateTime.now();

    long totalTime = Duration
      .between(timeOfSending, timeOfConfirmation)
      .toMillis();

    return totalTime;
  }

  /**
   * Executes the singleTransaction method multiple times,
   * saving the transaction times to a file
   *
   * @param web3j              The Web3j client used to interact with the Geth network
   * @param recievingAddress   String containing the ETH address of the recipient of the transaction to be made
   * @param value              BigDecimal value of the transaction, in WEI
   * @param credentials        The account credentials for the sender of the transaction, to allow the transaciton
   *                           to be made
   * @param file               File object where the results of each transaction should be written to
   */
  static void singleTransactionBenchmark(
    Web3j web3j,
    String recievingAddress,
    BigDecimal value,
    Credentials credentials,
    File file
  ) throws Exception {
    try (PrintWriter writer = new PrintWriter(file)) {
      StringBuilder titles = new StringBuilder();
      titles.append("Transaction No.");
      titles.append(',');
      titles.append("Time (Milliseconds)");

      writer.println(titles.toString());

      for (int i = 1; i < 101; i++) {
        StringBuilder content = new StringBuilder();
        content.append(String.valueOf(i));
        content.append(',');
        content.append(
          singleTransaction(web3j, recievingAddress, value, credentials)
        );
        content.append('\n');

        writer.write(content.toString());

        System.out.println("Written transaction " + (i));
      }

      writer.flush();
      writer.close();
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Executes the createClientRequest or getClientRequest methods,
   * depending on supplied arguments, a number of times. Saves the recorded
   * time to a file
   *
   * @param web3j              The Web3j client used to interact with the Geth network
   * @param deployedContract   The Record object representing the deployed smart contract
   * @param typeOfRequest      String containing either "createClient" or "getClient", indicating
   *                           which type of smart contract request to initiate
   * @param file               File object where the results of each transaction should be written to
   */
  static void smartContractTransactionBenchmark(
    Web3j web3j,
    Record deployedContract,
    String typeOfRequest,
    File file
  ) throws Exception {
    try (PrintWriter writer = new PrintWriter(file)) {
      StringBuilder titles = new StringBuilder();
      titles.append("Transaction No.");
      titles.append(',');
      titles.append("Time (Milliseconds)");

      writer.println(titles.toString());

      for (int i = 1; i < 101; i++) {
        StringBuilder content = new StringBuilder();
        content.append(String.valueOf(i));
        content.append(',');
        if (typeOfRequest.equals("createClient")) {
          content.append(createClientRequest(deployedContract, web3j));
        } else if (typeOfRequest.equals("getClient")) {
          content.append(getClientRequest(deployedContract));
        } else {
          throw new Exception("Incorrect request type");
        }
        content.append('\n');

        writer.write(content.toString());

        System.out.println("Written transaction " + (i));
      }

      writer.flush();
      writer.close();
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * The SendMultipleTransactions class enables concurrent execution
   * of the singleTransaction method, by implementing Runnable (so can be
   * used with threads). The returned values are stored in a BlockingQueue
   * to ensure results are stored properly, and no results are lost by attempting
   * to write directly to a file.
   *
   * @param web3j             The Web3j client used to interact with the Geth network
   * @param recievingAddress  String containing the ETH address of the recipient of the transaction to be made
   * @param value             BigDecimal value of the transaction, in WEI
   * @param credentials       The account credentials for the sender of the transaction, to allow the transaciton
   *                          to be made
   * @param queue             Queue object used to store the results of each thread's transaction
   */
  static class SendMultipleTransactions implements Runnable {

    private Web3j web3j;
    private String recievingAddress;
    private BigDecimal value;
    private Credentials credentials;
    private BlockingQueue<Long> queue;

    SendMultipleTransactions(
      Web3j web3j,
      String recievingAddress,
      BigDecimal value,
      Credentials credentials,
      BlockingQueue<Long> queue
    ) {
      this.web3j = web3j;
      this.recievingAddress = recievingAddress;
      this.value = value;
      this.credentials = credentials;
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        queue.put(
          singleTransaction(web3j, recievingAddress, value, credentials)
        );
      } catch (Exception e) {
        // Print any exceptions
        e.printStackTrace();
      }
    }
  }

  /**
   * The SendMultipleSmartContractTransactions class enables concurrent execution
   * of either the createClientRequest and getClientRequest methods, depending on
   * the supplied arugments, by implementing Runnable (so can be used with threads).
   * The returned values are stored in a BlockingQueue to ensure results are stored
   * properly, and no results are lost by attempting to write directly to a file.
   *
   * @param web3j             The Web3j client used to interact with the Geth network
   * @param deployedContract  The Record object representing the deployed smart contract
   * @param typeOfRequest     String containing either "createClient" or "getClient", indicating
   *                          which type of smart contract request to initiate
   * @param queue             Queue object used to store the results of each thread's smart contract
   *                          request
   */
  static class SendMultipleSmartContractTransactions implements Runnable {

    private Web3j web3j;
    private Record deployedContract;
    private String typeOfRequest;
    private BlockingQueue<Long> queue;

    SendMultipleSmartContractTransactions(
      Web3j web3j,
      Record deployedContract,
      String typeOfRequest,
      BlockingQueue<Long> queue
    ) {
      this.web3j = web3j;
      this.deployedContract = deployedContract;
      this.typeOfRequest = typeOfRequest;
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        if (typeOfRequest.equals("createClient")) {
          queue.put(createClientRequest(deployedContract, web3j));
        } else if (typeOfRequest.equals("getClient")) {
          queue.put(getClientRequest(deployedContract));
        } else {
          throw new Exception("Incorrect request type");
        }
      } catch (Exception e) {
        // Print any other exceptions
        e.printStackTrace();
      }
    }
  }

  /**
   * The WriteMultipleTransactions class takes the BlockingQueues from
   * SendMultipleTransactions and SendMultipleSmartContractTransactions and
   * writes the stored values to a specified file.
   *
   * @param queue   Queue object that contains the results to be written to a file
   * @param file    File object indicating which file the results should be written to
   */
  static class WriteMultipleTransactions implements Runnable {

    private BlockingQueue<Long> queue;
    private File file;

    WriteMultipleTransactions(BlockingQueue<Long> queue, File file) {
      this.queue = queue;
      this.file = file;
    }

    @Override
    public void run() {
      try (PrintWriter writer = new PrintWriter(file)) {
        int i = 1;
        StringBuilder titles = new StringBuilder();
        titles.append("Transaction No.");
        titles.append(',');
        titles.append("Time (Milliseconds)");
        writer.println(titles.toString());

        while (!queue.isEmpty()) {
          try {
            StringBuilder content = new StringBuilder();
            content.append(String.valueOf(i));
            content.append(',');
            content.append(queue.take());
            content.append('\n');

            writer.write(content.toString());

            System.out.println("Written transaction " + (i));
            i += 1;
          } catch (InterruptedException e) {
            // Print any interrupt exceptions
            e.printStackTrace();
          }
        }

        writer.flush();
        writer.close();
      } catch (FileNotFoundException e) {
        // Print any exceptions with finding the file
        e.printStackTrace();
      }
    }
  }

  /**
   * The multipleTransactionBenchmark method creates a number of threads
   * (based on value of numberOfTransactions), using the SendMultipleTransactions
   * class to benchmark multiple concurrent transactions. Another thread is then
   * created to collect the results using the WriteMultipleTransactions class.
   *
   * @param web3j                 The Web3j client used to interact with the Geth network
   * @param recievingAddress      String containing the ETH address of the recipient of the transaction to be made
   * @param credentials           The account credentials for the sender of the transaction, to allow the transaciton
   *                              to be made
   * @param file                  File object where the results of each transaction should be written to
   * @param numberOfTransactions  Integer value indicating the number of concurrent transactions to be made, each transaction
   *                              requires a new thread
   */
  static void multipleTransactionBenchmark(
    Web3j web3j,
    String recievingAddress,
    Credentials[] credentials,
    File file,
    int numberOfTransactions
  ) throws InterruptedException, IOException {
    if (numberOfTransactions >= credentials.length) {
      throw new IllegalArgumentException(
        "Number of transactions must be less/equal to number of credentials"
      );
    }

    BlockingQueue<Long> queue = new LinkedBlockingQueue<Long>(
      numberOfTransactions
    );

    Thread t[] = new Thread[numberOfTransactions];

    for (int i = 0; i < numberOfTransactions; i++) {
      t[i] =
        new Thread(
          new SendMultipleTransactions(
            web3j,
            recievingAddress,
            BigDecimal.valueOf(1),
            credentials[i],
            queue
          )
        );
      t[i].start();
    }

    for (int j = 0; j < numberOfTransactions; j++) {
      t[j].join();
    }

    Thread writeToFile = new Thread(new WriteMultipleTransactions(queue, file));

    writeToFile.start();
    writeToFile.join();
  }

  /**
   * The multipleSmartContractTransactionBenchmark method creates a number of threads
   * (based on value of numberOfTransactions), using the SendMultipleSmartContractTransactions
   * class to benchmark multiple concurrent smart contract requests (with the request sent depending on
   * the value of typeOfRequest). Another thread is then created to collect the results using the
   * WriteMultipleTransactions class.
   *
   * @param web3j                 The Web3j client used to interact with the Geth network
   * @param deployedContract      The list of records for the deployed smart contract, enabling concurrent interactions
   * @param typeOfRequest         String containing either "createClient" or "getClient", indicating
   *                              which type of smart contract request to initiate
   * @param file                  File object where the results of each transaction should be written to
   * @param numberOfTransactions  Integer value indicating the number of concurrent transactions to be made, each transaction
   *                              requires a new thread
   */
  static void multipleSmartContractTransactionBenchmark(
    Web3j web3j,
    Record[] deployedContract,
    String typeOfRequest,
    File file,
    int numberOfTransactions
  ) throws InterruptedException {
    BlockingQueue<Long> queue = new LinkedBlockingQueue<Long>(
      numberOfTransactions
    );

    Thread t[] = new Thread[numberOfTransactions];

    for (int i = 0; i < numberOfTransactions; i++) {
      t[i] =
        new Thread(
          new SendMultipleSmartContractTransactions(
            web3j,
            deployedContract[i],
            typeOfRequest,
            queue
          )
        );
      t[i].start();
    }

    for (int j = 0; j < numberOfTransactions; j++) {
      t[j].join();
    }

    Thread writeToFile = new Thread(new WriteMultipleTransactions(queue, file));

    writeToFile.start();
    writeToFile.join();
  }

  public static void main(String[] args) throws Exception {
    // Setting up files to be used by benchmark tests
    File[] transactionTestsFiles = new File[15];
    String[] transactionTestsFileNames = new String[] {
      "single",
      "50_concurrent",
      "100_concurrent",
      "150_concurrent",
      "200_concurrent",
    };

    for (int i = 0; i < transactionTestsFileNames.length; i++) {
      transactionTestsFiles[i] =
        new File(
          "./csvOutputs/geth/basicTransactions/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i].exists()) {
        transactionTestsFiles[i].createNewFile();
      }
      transactionTestsFiles[i + 5] =
        new File(
          "./csvOutputs/geth/smartContractCreateClient/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i + 5].exists()) {
        transactionTestsFiles[i + 5].createNewFile();
      }
      transactionTestsFiles[i + 10] =
        new File(
          "./csvOutputs/geth/smartContractGetClient/" +
          transactionTestsFileNames[i] +
          "Transactions.csv"
        );
      if (!transactionTestsFiles[i + 10].exists()) {
        transactionTestsFiles[i + 10].createNewFile();
      }
    }

    //Paths to files used in tests
    String walletsFilePath = "./wallets/gethWallets.csv";

    //Wallet password (same for all wallets)
    String accountPassword = "hello";

    //Start geth
    Thread geth = new Thread(new InitiateGeth());
    geth.start();

    //Reading in file of account wallets and creating credentials (used to send transactions) whilst geth starts
    String[] pathToWallets = new String[51];

    String line = "";
    try {
      BufferedReader br = new BufferedReader(new FileReader(walletsFilePath));
      while ((line = br.readLine()) != null) {
        pathToWallets = line.split(",");
      }
      br.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

    Credentials[] credentials = new Credentials[pathToWallets.length];

    for (int i = 0; i < pathToWallets.length; i++) {
      pathToWallets[i] = pathToWallets[i].replace("\\", "\\\\");
      pathToWallets[i] = pathToWallets[i].replaceAll("^\"|\"$", "");
      credentials[i] =
        WalletUtils.loadCredentials(accountPassword, pathToWallets[i]);
      System.out.println("Credential " + (i + 1) + " loaded.");
    }

    //Connect to geth
    Web3j client = Web3j.build(new HttpService());

    //Start mining node
    Thread node = new Thread(new InitiateNode());
    node.start();

    // Set up contract gas provider
    final BigInteger gasPrice = BigInteger.valueOf(1000000000);
    final BigInteger gasLimit = BigInteger.valueOf(2000000);
    final ContractGasProvider gasProvider = new StaticGasProvider(
      gasPrice,
      gasLimit
    );

    // Use contract gas provider and credentials to deploy contract to geth chain
    Record contract = Record.deploy(client, credentials[0], gasProvider).send();
    String contractAddress = contract.getContractAddress();

    //Each ETH account needs it's own Record to interact concurrent, otherwise GETH client thinks its the same transaction being sent
    final Record deployedContract[] = new Record[200];

    for (int i = 0; i < 200; i++) deployedContract[i] =
      Record.load(contractAddress, client, credentials[i], gasProvider);

    // Single and concurrent tests for basic transactions

    singleTransactionBenchmark(
      client,
      credentials[1].getAddress(),
      BigDecimal.valueOf(1L),
      credentials[0],
      transactionTestsFiles[0]
    );

    for (int i = 50; i < 201; i += 50) {
      multipleTransactionBenchmark(
        client,
        credentials[200].getAddress(),
        credentials,
        transactionTestsFiles[i / 50],
        i
      );
    }

    // Single and concurrent tests for createClient and getClient smart contract functions

    smartContractTransactionBenchmark(
      client,
      deployedContract[0],
      "createClient",
      transactionTestsFiles[5]
    );

    smartContractTransactionBenchmark(
      client,
      deployedContract[0],
      "getClient",
      transactionTestsFiles[10]
    );

    for (int i = 300; i < 451; i += 50) {
      multipleSmartContractTransactionBenchmark(
        client,
        deployedContract,
        "createClient",
        transactionTestsFiles[i / 50],
        i - 250
      );
    }

    for (int i = 550; i < 701; i += 50) {
      multipleSmartContractTransactionBenchmark(
        client,
        deployedContract,
        "getClient",
        transactionTestsFiles[i / 50],
        i - 500
      );
    }

    //End the processes
    InitiateNode.setFinished(true);
    InitiateGeth.setFinished(true);

    //Close any background GETH processes
    Runtime.getRuntime().exec("taskkill /F /IM geth.exe");

    geth.join();
    node.join();
  }
}
