// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/** 
 * @title  Contract containing base structure for a client record 
 * @author James Lee
 */
contract RecordBase{

    /** 
     * @dev Structure of a Client 
     * @param clientId     Identifier used to retrieve specific client record
     * @param clientName   Name of the client to store in the record
     * @param recordOwner  The ETH address of the user that created the client record,
     *                     used to ensure only the record creator can edit the record
     */
    struct Client{
        uint256 clientId;
        string clientName;
        address recordOwner;
    }

}

/** 
 * @title  Contract implementing client record system
 * @author James Lee
 */
contract Record is RecordBase{

    Client[] private clients;

    //A mapping is created, used to check whether a client exists 
    //when getClient method is called
    mapping(uint256 => bool) clientExists;

    error InsufficientPrivileges(address walletId);

    error ClientNotFound(address walletId);

    /**
     * @notice Ensure that only the record owner can edit the record
     * @dev    If the ETH address of the request sender is not equal to the 
     *         record owner, it throws Insufficient Privileges error back to 
     *         the user
     * @param _clientId  ID of the record that is trying to be edited
     */
    modifier onlyAuthorisedSenders(uint256 _clientId) {
        if (msg.sender !=  clients[_clientId].recordOwner) {
            revert InsufficientPrivileges(msg.sender);
        }
        /// Else, return back to calling function
        _;
    }

    /**
     * @notice Check if a clientId exists within the records
     * @dev    If the client ID does not exist within the mapping, 
     *         throw Client Not Found error back to the user
     * @param _clientId  ID of the record that is trying to be edited/viewed
     */
    modifier existingClients(uint256 _clientId) {
        if (!clientExists[_clientId]){
            revert ClientNotFound(msg.sender);
        } 
        /// Else, return back to calling function
        _;       
    }

    /**
     * @notice Create a new client record
     * @dev    Pushes a new client record to the clients array, and
     *         signals that the client exists within the clientExists
     *         mapping by setting a "true" value for the client Id.
     * @param _clientName String containing the name of the client
     *                    to be stored in the record    
     */
    function createClient(string memory _clientName) public{
        clients.push(
            Client({
                clientId: clients.length,
                clientName: _clientName,
                recordOwner: msg.sender
            })
        );

        clientExists[clients.length - 1] = true;

    }

    /**
     * @notice Update the name on a client record
     * @dev    Checks if the client exists using existingClients modifier, and
     *         checks if the message sender has permission to update the name using
     *         onlyAuthorisedSenders modifier
     * @param _clientId Identifier of the record to update the name of
     * @param _newName  String containing the new name to update to
     */
    function updateClientName(uint256 _clientId, string memory _newName) public existingClients(_clientId) onlyAuthorisedSenders(_clientId) {
        clients[_clientId].clientName = _newName;
    }

    /**
     * @notice Get the record of a specific client ID
     * @dev Checks if the client exists using existingClients modifier
     * @param _clientId Identifier of the record to get
     * @return Client record for the requested client ID
     */
    function getClient(uint256 _clientId) public view existingClients(_clientId) returns(Client memory) {
        Client memory client = clients[_clientId];

        return client;
    }
}