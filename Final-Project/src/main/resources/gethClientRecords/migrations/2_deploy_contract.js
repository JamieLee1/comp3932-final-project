
var Record = artifacts.require("Record");
var RecordBase = artifacts.require("RecordBase");

module.exports = function(deployer) {
   deployer.deploy(RecordBase);
   deployer.deploy(Record);
};